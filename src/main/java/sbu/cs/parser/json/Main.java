package sbu.cs.parser.json;

public class Main {

    public static void main(String[] args){
        Json jsn = JsonParser.parse("{\"string\": null " +
                ", \"string2\": \"ooooooooo\"" +
                ", \"int\":21" +
                ", \"double\" : 123.456" +
                ", \"boolF\": false" +
                ", \"boolT\" :            true" +
                ", \"arr\" : [\"bhfbh\", \"1gbtbtrn4\", \" gfbber17\"]}");

        System.out.println(jsn.getStringValue("string"));
        System.out.println(jsn.getStringValue("string2"));
        System.out.println(jsn.getStringValue("int"));
        System.out.println(jsn.getStringValue("double"));
        System.out.println(jsn.getStringValue("boolF"));
        System.out.println(jsn.getStringValue("boolT"));
        System.out.println(jsn.getStringValue("arr"));
        System.out.println(jsn.getStringValue("age"));


    }
}
