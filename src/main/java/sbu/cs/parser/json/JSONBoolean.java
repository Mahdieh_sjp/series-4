package sbu.cs.parser.json;

public class JSONBoolean{
    String key;
    boolean value;

    public JSONBoolean(String key, boolean value) {
        this.key = key;
        this.value = value;
    }
}
