package sbu.cs.parser.json;

public class Cursor {

    public static final char OPEN_ACCOLADE = '{';
    public static final char CLOSED_ACCOLADE = '}';
    public static final char COLON = ':';
    public static final char DOUBLE_QUOTATION = '"';
    public static final char COMMA = ',';
    public static final char OPEN_BRACKET = '[';
    public static final char CLOSED_BRACKET = ']';

    public static final String SPECIAL_CHARS = "{}:\",[]";
    public static final String INT_CHARS = "-0123456789";
    public static final String DOUBLE_CHARS = "-.0123456789";

    String data;
    int index = 0;
    boolean betweenDoubleQuotation = false;


    public Cursor(String data) {
        if(data == null)
            throw new IllegalArgumentException("Null Json Object");
        this.data = data;
    }


    public String keyReader() {
        String key = stringReader();
        colonReader();
        return key;
    }


    public void openAccoladeReader() {
        char ch = read();
        if (ch != OPEN_ACCOLADE) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void closedAccoladeReader() {
        char ch = read();
        if (ch != CLOSED_ACCOLADE) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void openBracketReader() {
        char ch = read();
        if (ch != OPEN_BRACKET) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void closedBracketReader() {
        char ch = read();
        if (ch != CLOSED_BRACKET) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void colonReader() {
        char ch = read();
        if (ch != COLON) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void commaReader() {
        char ch = read();
        if (ch != COMMA) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void quotationReader() {
        char ch = read();
        if (ch != DOUBLE_QUOTATION) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public String stringReader() {
        quotationReader();
        betweenDoubleQuotation = true;
        StringBuilder builder = new StringBuilder();
        char ch;
        while ((ch = read()) != DOUBLE_QUOTATION) {
            builder.append(ch);
        }
        String str = builder.toString();
        betweenDoubleQuotation = false;
        return str;
    }


    public int intReader() {
        StringBuilder builder = new StringBuilder();
        char ch = nexChar();

        while (ch != COMMA && ch != CLOSED_ACCOLADE && ch != CLOSED_BRACKET) {
            read();

            if (INT_CHARS.contains(ch + "")) {
                builder.append(ch);
            } else {
                throw new IllegalArgumentException("invalid input");
            }
            ch = nexChar();
        }
        return Integer.parseInt(builder.toString());
    }


    public double doubleReader() {
        StringBuilder builder = new StringBuilder();
        char ch = nexChar();

        while (ch != COMMA && ch != CLOSED_ACCOLADE && ch != CLOSED_BRACKET) {
            read();
            builder.append(ch);
            ch = nexChar();
        }
        return Double.parseDouble(builder.toString());
    }


    public boolean booleanReader() {
        StringBuilder builder = new StringBuilder();
        betweenDoubleQuotation = true;
        char ch = nexChar();
        while (ch != COMMA && ch != CLOSED_ACCOLADE && ch != CLOSED_BRACKET) {
            read();
            ch = nexChar();
            if (ch == ' '){
                continue;
            }
            builder.append(ch);
        }
        builder.deleteCharAt(builder.length() - 1);
        betweenDoubleQuotation = false;
        return Boolean.parseBoolean(builder.toString());
    }


    public void nullReader(){
        StringBuilder builder = new StringBuilder();
        betweenDoubleQuotation = true;
        char ch = nexChar();
        while (ch != COMMA && ch != CLOSED_ACCOLADE && ch != CLOSED_BRACKET) {
            read();
            ch = nexChar();
            builder.append(ch);
        }
        builder.deleteCharAt(builder.length() - 1);
        betweenDoubleQuotation = false;

        if(!builder.toString().toLowerCase().trim().equals("null")){
            throw new IllegalArgumentException("invalid input");
        }
    }


    private boolean validChar(char ch) {
        return betweenDoubleQuotation || SPECIAL_CHARS.contains("" + ch) || DOUBLE_CHARS.contains(ch + "");
    }


    public char nexChar() {
        if(betweenDoubleQuotation) {
            return data.charAt(index);
        }
        int nextNonemptyIndex = index;
        char ch = data.charAt(nextNonemptyIndex);

        while (ch == ' ' || ch == '\n') {
            nextNonemptyIndex++;
            ch = data.charAt(nextNonemptyIndex);
        }
        return ch;
    }


    public boolean isNextDouble() {
        int tempIdx = index;
        char ch = read();

        while (ch != COMMA && ch != CLOSED_ACCOLADE && ch != CLOSED_BRACKET) {
            if(ch == '.'){
                index = tempIdx;
                return true;
            }
            ch = read();
        }
        index = tempIdx;
        return false;
    }


    private char read(){
        char ch;
        if (!betweenDoubleQuotation) {
            while ((ch = data.charAt(index)) == ' ' || ch == '\n') {
                index++;
            }
            if (validChar(ch)) {
                index++;
                return ch;
            } else {
                throw new IllegalArgumentException("invalid input");
            }
        }
        else {
            ch = data.charAt(index);
            index++;
            return ch;
        }
    }

}
