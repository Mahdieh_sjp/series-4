package sbu.cs.parser.json;

public class JSONDouble{
    String key;
    Double value;

    public JSONDouble(String key, Double value) {
        this.key = key;
        this.value = value;
    }
}
