package sbu.cs.parser.json;

import java.util.ArrayList;

public class JsonParser {


    public static Json parse(String data) {
        Cursor cursor = new Cursor(data
                .replaceAll("\\n","")
                .replaceAll("\\t", ""));
        return parseObject(cursor);
    }


    private static ArrayList parseArray(Cursor cursor) {
        cursor.openBracketReader();

        ArrayList<Object> array = new ArrayList<>();
        array = parseArrayItems(cursor, array);
        cursor.closedBracketReader();
        return array;
    }


    private static ArrayList parseArrayItems(Cursor cursor, ArrayList array) {

        while (cursor.nexChar() != Cursor.CLOSED_BRACKET) {

            char ch = cursor.nexChar();

            if (ch == Cursor.DOUBLE_QUOTATION) {
                String content = cursor.stringReader();
                array.add(content);
            }
            else if (ch == Cursor.OPEN_BRACKET) {
                ArrayList nestedArray = parseArray(cursor);
                array.add(nestedArray);
            }
            else if (Cursor.INT_CHARS.contains(ch + "")) {
                boolean isUpcomingDouble = cursor.isNextDouble();

                if (isUpcomingDouble) {
                    double d = cursor.doubleReader();
                    array.add(d);
                }
                else {
                    int number = cursor.intReader();
                    array.add(number);
                }
            } else {
                boolean b = cursor.booleanReader();
                array.add(b);
            }
            ch = cursor.nexChar();
            if (ch != Cursor.CLOSED_BRACKET) {
                cursor.commaReader();
            }
        }
        return array;
    }


    private static Json parseObject(Cursor cursor) {
        cursor.openAccoladeReader();
        Json json = new Json(new JSONObject());
        parseKeyValue(cursor, json.jsonObject);
        return json;
    }


    private static void parseKeyValue(Cursor cursor, JSONObject jsonObject) {
        while (cursor.nexChar() != Cursor.CLOSED_ACCOLADE) {
            readKeyValue(cursor, jsonObject);
            char ch = cursor.nexChar();
            if (ch != Cursor.CLOSED_ACCOLADE) {
                cursor.commaReader();
            }
        }
        cursor.closedAccoladeReader();
    }


    private static void readKeyValue(Cursor cursor, JSONObject jsonObject) {
        String key = cursor.keyReader();
        char nextChar = cursor.nexChar();

        if (nextChar == Cursor.DOUBLE_QUOTATION) {
            String value = cursor.stringReader();
            jsonObject.addJsonString(key, value);
        }
        else if (Cursor.INT_CHARS.contains(nextChar + "")) {
            if (cursor.isNextDouble()) {
                double dbl = cursor.doubleReader();
                jsonObject.addJsonDouble(key, dbl);
            }
            else {
                int number = cursor.intReader();
                jsonObject.addJsonInt(key, number);
            }
        }
        else if (nextChar == Cursor.OPEN_BRACKET) {
            ArrayList<Object> array = parseArray(cursor);
            jsonObject.addJsonArray(key, array);
        }
        else if (nextChar == 'n' || nextChar == 'N'){
            cursor.nullReader();
            jsonObject.addJsonNull(key);
        }
        else {
            boolean bool = cursor.booleanReader();
            jsonObject.addJsonBoolean(key, bool);
        }
    }

}
