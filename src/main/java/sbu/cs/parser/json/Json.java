package sbu.cs.parser.json;

public class Json implements JsonInterface {

    public JSONObject jsonObject;

    public Json(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public String getStringValue(String key) {

        for(int i = 0; i < jsonObject.items.size(); i++){

            if(jsonObject.items.get(i) instanceof JSONBoolean) {
                if (((JSONBoolean) jsonObject.items.get(i)).key.equals(key)) {
                    boolean jb = ((JSONBoolean) jsonObject.items.get(i)).value;
                    return String.valueOf(jb);
                }
            }


            else if(jsonObject.items.get(i) instanceof JSONInt){
                if(((JSONInt) jsonObject.items.get(i)).key.equals(key)){
                    int ji = ((JSONInt) jsonObject.items.get(i)).value;
                    return String.valueOf(ji);
                }
            }

            else if(jsonObject.items.get(i) instanceof JSONDouble){
                if(((JSONDouble) jsonObject.items.get(i)).key.equals(key)){
                    double jd = ((JSONDouble) jsonObject.items.get(i)).value;
                    return String.valueOf(jd);
                }
            }

            else if(jsonObject.items.get(i) instanceof JSONString){
                if(((JSONString) jsonObject.items.get(i)).key.equals(key)){
                    String js = ((JSONString) jsonObject.items.get(i)).value;
                    if(!js.equals("")) {
                        return js;
                    }
                    return "null";
                }
           }
            else if(jsonObject.items.get(i) instanceof JSONArray){
                if(((JSONArray) jsonObject.items.get(i)).key.equals(key)) {
                    System.out.println(((JSONArray) jsonObject.items.get(i)).value.get(0));
                    String output = "[";
                    for(Object o : (((JSONArray) jsonObject.items.get(i)).value)){
                        output += o.toString() + ", ";
                    }
                    output = output.substring(0, output.length() - 2) + "]";
                    return output;
                }
            }
        }
        return "no such key";
    }
}
