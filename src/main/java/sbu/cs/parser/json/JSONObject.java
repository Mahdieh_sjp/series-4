package sbu.cs.parser.json;

import java.util.ArrayList;


public class JSONObject{


    public JSONObject() {
    }

    ArrayList<Object> items = new ArrayList();


    public void addJsonInt(String key, int value) {
        items.add(new JSONInt(key, value));
    }


    public void addJsonDouble(String key, double value) {
        items.add(new JSONDouble(key, value));
    }


    public void addJsonBoolean(String key, boolean value) {
        items.add(new JSONBoolean(key, value));
    }


    public void addJsonString(String key, String value) {
        items.add(new JSONString(key, value));
    }


    public void addJsonArray(String key, ArrayList value) {
        items.add(new JSONArray(key, value));
    }


    public void addJsonNull(String key) {
        items.add(new JSONString(key, new String()));
    }
}
