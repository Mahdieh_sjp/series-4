package sbu.cs.parser.json;

public class JSONInt{
    String key;
    int value;

    public JSONInt(String key, int value) {
        this.key = key;
        this.value = value;
    }
}
