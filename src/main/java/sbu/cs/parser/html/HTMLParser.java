package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.List;

public class HTMLParser {


    public static Node parse(String document) {
        Cursor cursor = new Cursor(document
                .replaceAll("\n", ""));
        return tagParser(cursor);
    }


    private static Node tagParser(Cursor cursor){
        cursor.lessThanReader();
        Node node = new Node();

        if(cursor.nexChar() == Cursor.SLASH){
            singletonTagParser(cursor, node);
        }
        else { normalTagParser(cursor, node); }

        return node;
    }


    private static void normalTagParser(Cursor cursor, Node node){
        getTagType(cursor, node);

        cursor.greaterThanReader();

        insideStringParser(cursor, node);

        cursor.lessThanReader();
        cursor.slashReader();
        cursor.tagReader();
        cursor.greaterThanReader();

        childrenParser(node.getInsideString(), node);
    }


    private static void singletonTagParser(Cursor cursor, Node node){
        cursor.slashReader();

        getTagType(cursor, node);

        cursor.greaterThanReader();
    }


    private static void getTagType(Cursor cursor, Node node){
        node.setTagType(cursor.tagReader());

        if(cursor.nexChar() != Cursor.GREATER_THAN){
            attributesParser(cursor, node);
        }
    }


    private static void attributesParser(Cursor cursor, Node node){

        while(cursor.nexChar() != Cursor.GREATER_THAN){
            node.addAttribute(cursor.keyReader(), cursor.valueReader());

            cursor.quotationReader();
        }
    }


    private static void insideStringParser(Cursor cursor, Node node){
        String insideString = cursor.insideStringReader(node.getTagType());
        node.setInsideString(insideString);

    }


    private static void childrenParser(String document, Node node){
        if(document.contains("<")){
            List<Node> children = new ArrayList<Node>();
            Cursor cursor = new Cursor(document);

            while (cursor.getIndex() < document.trim().length()){
                children.add(tagParser(cursor));
            }

            node.setChildren(children);
        }


    }


}
