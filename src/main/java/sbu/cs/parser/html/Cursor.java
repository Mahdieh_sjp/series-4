package sbu.cs.parser.html;

public class Cursor {

    public static final char LESS_THAN = '<';
    public static final char GREATER_THAN = '>';
    public static final char SLASH = '/';
    public static final char EQUAL = '=';
    public static final char DOUBLE_QUOTATION = '"';



    private String data;
    private int index;

    public Cursor(String data) {
        if(data == null)
            throw new IllegalArgumentException("Null HTML doc");
        this.data = data;
        index = 0;
    }


    public int getIndex() {
        return index;
    }


    public void lessThanReader() {
        char ch = read();
        if (ch != LESS_THAN) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void greaterThanReader() {
        char ch = read();
        if (ch != GREATER_THAN) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void slashReader() {
        char ch = read();
        if (ch != SLASH) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void equalReader() {
        char ch = read();
        if (ch != EQUAL) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public void quotationReader() {
        char ch = read();
        if (ch != DOUBLE_QUOTATION) {
            throw  new IllegalArgumentException("invalid input");
        }
    }


    public String tagReader() {
        char ch = nexChar();
        StringBuilder builder = new StringBuilder();

        while(ch != ' ' &&ch != GREATER_THAN){
            builder.append(ch);
            read();
            ch = data.charAt(index);
        }

        return builder.toString();
    }


    public String keyReader(){
        char ch = nexChar();
        StringBuilder builder = new StringBuilder();

        while(ch != EQUAL){
            ch = read();
            builder.append(ch);
            ch = nexChar();
        }
        return builder.toString();
    }


    public String valueReader() {
        equalReader();
        quotationReader();

        StringBuilder builder = new StringBuilder();
        char ch = nexChar();

        while(ch != DOUBLE_QUOTATION){
            builder.append(ch);
            index++;
            ch = data.charAt(index);
        }

        return builder.toString();
    }


    public String insideStringReader(String tagType){
        int endIndex = data.indexOf("</" + tagType, index);
        String insideString = data.substring(index, endIndex);
        index = endIndex;
        return insideString;
    }


    public char nexChar() {
        int nextNonemptyIndex = index;
        char ch = data.charAt(nextNonemptyIndex);

        while (ch == ' ' || ch == '\n') {
            nextNonemptyIndex++;
            ch = data.charAt(nextNonemptyIndex);
        }
        return ch;
    }


    private char read(){
        char ch;
        while ((ch = data.charAt(index)) == ' ' || ch == '\n') {
            index++;
        }
        index++;
        return ch;

    }

}
