package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node implements NodeInterface {



    private List<Node> children;
    private String tagType;
    private String insideString;
    private Map<String, String> attributes;


    Node(){
        children = new ArrayList();
        attributes = new HashMap<>();
    }


    public void setChildren(List<Node> children) {
        this.children = children;
    }


    public String getTagType() {
        return tagType;
    }


    public void setTagType(String tagType) {
        this.tagType = tagType;
    }


    public String getInsideString() {
        return insideString;
    }


    public void setInsideString(String insideString) {
        this.insideString = insideString;
    }


    public Map<String, String> getAttributes() {
        return attributes;
    }


    public void addAttribute(String key, String value) {
        this.attributes.put(key, value);
    }


    @Override
    public String getStringInside() {
        return this.insideString;
    }


    @Override
    public List<Node> getChildren() {
        return this.children;
    }


    @Override
    public String getAttributeValue(String key) {
        return attributes.get(key);
    }
}
